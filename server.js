/* WebSockets setup */

var express = require("express");
var app = express();
var httpServer = require("http").createServer(app);
var socketio = require("socket.io");
var io = socketio.listen(httpServer);
var less = require('less-middleware');
var path = require('path');
app.use(express.static("app"));
app.use(express.static("bower_components"));

app.use(less({
        src: path.join(__dirname, 'app/less'),
        dest: path.join(__dirname, 'app/css'),
        prefix: '/css',
        compress: true
    }));

/* End of WebSockets setup */

/* Variables and prototypes */

var moviesList = [],
    timer,
    timerTimeout1,
    timerTimeout2,
    seconds = 0,
    playing = false,
    currentMovie,
    uniqueId = 1;

var Movie = function(videoId, duration) {
    this.id = generateId();
    this.videoId = videoId;
    this.duration = duration;
    this.rates = [];
    this.rating = 0;

    this.calcRating = function() {
        if (this.rates.length === 0) return;
        var i, sum, length = this.rates.length;
        for (i = 0, sum = 0; i < length; i++) {
            sum += this.rates[i].rate;
        }
        this.rating = sum / length;
    };

    this.calcThumbUps = function() {
        if (this.rates.length === 0) return 0;
        var i, sum, length = this.rates.length;
        for (i = 0, count = 0; i < length; i++) {
            if(this.rates[i].rate == 1) count++;
        }
        return count;
    };
};

var Rate = function(rate, user) {
    this.user = user;
    this.rate = rate;
};

/* End of variables and prototypes */


/* WebSockets logic */

var onPause = function() {
    if(currentMovie === undefined) return;
    playing = false;
    clearTimeout(timerTimeout1);
    clearTimeout(timerTimeout2);
    clearInterval(timer);
};

var onResume = function() {
    beginTransmission(currentMovie, seconds);
};

var onSkipMovie = function() {
    stopIncrementSeconds();
    emitNewMovie();
};

var generateId = function() {
    var result = "movie" + uniqueId.toString();
    uniqueId++;
    return result;
};

var findMovieById = function(id) {
    for (var i = 0, len = moviesList.length; i < len; i++) {
        if (moviesList[i].id === id) {
            return moviesList[i];
        }
    }
};

var startIncrementSeconds = function() {
    seconds = seconds + 1;
};

var stopIncrementSeconds = function() {
    clearInterval(timer);
    seconds = 0;
    currentMovie = undefined;
    playing = false;
};

var beginTransmission = function(chosenMovie, second) {
    if(chosenMovie === undefined) return;
    var duration = second !== undefined ? chosenMovie.duration - second : chosenMovie.duration;
    playing = true;

    timer = setInterval(startIncrementSeconds, 1000);
    timerTimeout1 = setTimeout(stopIncrementSeconds, duration * 1000);
    timerTimeout2 = setTimeout(emitNewMovie, duration * 1000);
};

var emitNewMovie = function() {
    if (moviesList.length === 0) return;
    currentMovie = moviesList.shift();
    io.sockets.emit('nextVideo', currentMovie);
    beginTransmission(currentMovie);
    emitList();
};

var emitListForClient = function(socket) {
    socket.emit('changedList', moviesList);
};

var emitList = function() {
    io.sockets.emit('changedList', moviesList);
};

var onMovieAdded = function(data) {
    var newMovie = new Movie(data.vidId, data.dur);
    moviesList.push(newMovie);

    if (moviesList.length === 1 && !playing) {
        emitNewMovie();
    }
    emitList();
};

var onSync = function(socket) {
    emitHello(this);
};

var emitHello = function(socket) {
    var data = {
        "currentMovie": currentMovie,
        "seconds": seconds
    };
    socket.emit("hello", data);
    emitListForClient(socket);
};

var sortByRating = function(a, b) {
    if(b.rating - a.rating > 0) 
        return 1;
    else if(b.rating - a.rating < 0)
        return -1;
    else 
        return b.calcThumbUps() - a.calcThumbUps();
};

var onVoted = function(data) {
    var movie = findMovieById(data.id);
    var rate = new Rate(data.vote, data.user);
    movie.rates.push(rate);
    movie.calcRating();
    moviesList.sort(sortByRating);
    emitList();
};

io.sockets.on("connection", function(socket) {

    socket.on("movieAdded", onMovieAdded);

    socket.on("sync", onSync);

    socket.on("voted", onVoted);

    socket.on("error", function(err) {
        console.dir(err);
    });

    socket.on("skipRequest", onSkipMovie);

    socket.on("pause", onPause);

    socket.on("resume", onResume);

    socket.emit('hello world', socket.id);

});
httpServer.listen((process.env.PORT || 5000), function() {
    console.log("Ready.");
});

/* End of WebSockets logic */
