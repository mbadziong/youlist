# YouList

Aplikacja internetowa pozwalająca na wspólne przeglądanie filmów udostępnianych w Internecie
na serwisach takich jak Youtube.

W aplikacji może być aktywnych kilku użytkowników jednocześnie.
Każdy użytkownik ma ten sam widok aplikacji.
Każdy może wrzucać swoje propozycje na listę.
Lista może być modyfikowana poprzez oceny wrzuconych filmików.
Priorytet kolejności na liście zmienia się razem z ocenami.
Utwory nisko ocenione lądują na końcu, analogicznie wysoko ocenione pną się ku górze.
W przypadku braku oceniania kolejność definiuje czas wrzucenia propozycji.

## Instalacja:
(zakładam, że grunt i bower są zainstalowane)

###Wpisujemy w konsolę:

* npm install
* grunt
* npm start