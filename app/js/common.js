/*jshint globalstrict: true, devel: true, browser: true, jquery: true */
/* global YT, io */
"use strict";

var socket,
    player,
    moviesList = [],
    currentMovie,
    seconds,
    clientId,
    lock = false,
    paused = false;

/* app logic */

var onYouTubeIframeAPIReady = function() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: '',
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
};

var onPlayerReady = function(event) {
    event.target.clearVideo();
};

var checkCurrentMovie = function() {
    if (currentMovie !== undefined) {
        playNow(currentMovie.videoId, seconds);
    }
};

var onPlayerStateChange = function(event) {
    if (event.data == YT.PlayerState.ENDED) {
        $("#playing").fadeOut(200);
    } else if(event.data == YT.PlayerState.PAUSED) {
        //socket.emit("pause", '');
        paused = true;
    } else if(event.data == YT.PlayerState.PLAYING && paused === true) {
        //socket.emit("resume", '');
        paused = false;
    }
};

var stopVideo = function() {
    player.stopVideo();
};

var getVideoTitle = function(videoId, callback) {
    $.getJSON('https://gdata.youtube.com/feeds/api/videos/' + videoId + '?v=2&alt=json', function(data) {
        var title = data.entry.title.$t;
        callback(title);
    });
};

var checkIfAlreadyRated = function(rates) {
    var rated = false;
    var rate = 0;
    for (var i = 0, len = rates.length; i < len; i++) {
        if (clientId === rates[i].user) {
            rated = true;
            rate = rates[i].rate;
            break;
        }
    }
    return {
        "rated": rated,
        "rate": rate
    };
};

var addVidToList = function(data) {
    if (data.length === 0) return;
    var current = data.shift();
    var rateInfo = checkIfAlreadyRated(current.rates);

    getVideoTitle(current.videoId, function(title) {
        var li = $("<li/>");
        var borderDiv = $("<div/>").css("border", "1px solid grey").css("border-radius", "5px");
        var titleDiv = $("<div/>").css("text-align", "center");
        var titleParagraph = $("<p/>").css("color", "grey").text(title);
        var vidImgPreview = $("<img/>").attr("src", "http://img.youtube.com/vi/" + current.videoId + "/0.jpg").css("width", "100px");
        var divRating = $("<div/>").addClass("rating");
        var imgLike = $("<img/>");
        var imgDislike = $("<img/>");
        var imgPreview = $("<img/>");

        $("#vidList").append(li);
        $(li).append(borderDiv);
        $(borderDiv).append(titleDiv);
        $(titleDiv).append(titleParagraph).append(vidImgPreview).append(divRating);

        if (rateInfo.rated) {
            if (rateInfo.rate === 1) {
                $(divRating).append(imgLike);
                $(imgLike).attr("id", "like" + current.id).attr("src", "../images/Liked.png").addClass("liked");
            } else {
                $(divRating).append(imgDislike);
                $(imgDislike).attr("id", "dislike" + current.id).attr("src", "../images/Disliked.png").addClass("disliked");
            }
        } else {
            $(divRating).append(imgLike).append(imgDislike).append(imgPreview);
            $(imgLike).attr("id", "like" + current.id).attr("src", "../images/Like.png").addClass("like");
            $(imgDislike).attr("id", "dislike" + current.id).attr("src", "../images/Dislike.png").addClass("dislike");
            $(imgPreview).addClass("preview").attr("data-vid", current.videoId).attr("src", "../images/Preview_grey.png");
        }
        if (data.length > 0) addVidToList(data);
    });
};

var changeVideo = function(videoId) {
    player.loadVideoById(videoId, 0, "large");
};

var getVideoId = function(url) {
    return url.split("v=")[1];
};

var playNow = function(vidId, seconds) {
    changeVideo(vidId);
    getVideoTitle(vidId, function(title) {
        var currentMovieTitle = $("#playing");
        if (currentMovieTitle.text() !== "")
            $("#playing").fadeOut(200);
        if (lock === true) {
            title = "Preview: " + title;
        }
        $("#playing").text(title).fadeIn(2000);
    });
    player.playVideo();
    if (seconds !== undefined) {
        setTimeout(function() {
            player.seekTo(seconds + 1, true);
        }, 1000);
    }
};

/* End of app logic */

/* Server methods */

var emitMovieToServer = function(vidId, dur) {
    var data = {
        "vidId": vidId,
        "dur": dur
    };
    socket.emit('movieAdded', data);
};

var emitVoteToServer = function(id, vote, userId) {
    var data = {
        "id": id,
        "vote": vote,
        "user": userId
    };
    socket.emit('voted', data);
};

var onNextVideo = function(data) {
    if (lock === true) return;
    var vidId = data.videoId;
    playNow(vidId);
};

var onChangedList = function(data) {
    $('#vidList').empty();
    addVidToList(data);
};

var onHello = function(data) {
    seconds = data.seconds;
    currentMovie = data.currentMovie;
    checkCurrentMovie();
};

/* End of server methods */

/* Events */

var synchronizeClick = function() {
    lock = false;
    socket.emit('sync', "");
};

var addNextClick = function() {
    var uInput = $('#nextVid');
    var vidUrl = uInput.val();
    var vidId = getVideoId(vidUrl);
    var dur;
    $.getJSON('https://gdata.youtube.com/feeds/api/videos/' + vidId + '?v=2&alt=json', function(data) {
        dur = data.entry.media$group.media$content[0].duration;
        emitMovieToServer(vidId, dur);
    });
    uInput.val('').focus();
};

var skipClick = function() {
    socket.emit('skipRequest', clientId);
};

var likeOnOver = function() {
    $(this).attr("src", "../images/Liked.png");
};

var likeOnOut = function() {
    $(this).attr("src", "../images/Like.png");
};

var likeOnClick = function() {
    $(this).attr("src", "../images/Liked.png").removeClass("like");
    var id = this.id.slice(9);
    $('#dislikemovie' + id).hide();
    emitVoteToServer("movie" + id, 1, clientId);
};

var dislikeOnOver = function() {
    $(this).attr("src", "../images/Disliked.png");
};

var dislikeOnOut = function() {
    $(this).attr("src", "../images/Dislike.png");
};

var dislikeOnClick = function() {
    $(this).attr("src", "../images/Disliked.png").removeClass("dislike");
    var id = this.id.slice(12);
    $('#likemovie' + id).hide();
    emitVoteToServer("movie" + id, -1, clientId);
};

var previewOnOver = function() {
    $(this).attr("src", "../images/Preview.png");
};

var previewOnOut = function() {
    $(this).attr("src", "../images/Preview_grey.png");
};

var previewOnClick = function() {
    var vidId = $(this).attr("data-vid");
    lock = true;
    playNow(vidId, 0);
};

/* End of Events */

$(document).ready(function() {

    $('#addNext').click(addNextClick);

    $('#sync').click(synchronizeClick);

    $('#skip').click(skipClick);

    $(document).on('mouseover', '.like', likeOnOver);

    $(document).on('mouseout', '.like', likeOnOut);

    $(document).on('click', '.like', likeOnClick);

    $(document).on('mouseover', '.dislike', dislikeOnOver);

    $(document).on('mouseout', '.dislike', dislikeOnOut);

    $(document).on('click', '.dislike', dislikeOnClick);

    $(document).on('mouseover', '.preview', previewOnOver);

    $(document).on('mouseout', '.preview', previewOnOut);

    $(document).on('click', '.preview', previewOnClick);

    socket = io.connect('http://' + location.host);

    socket.on('nextVideo', onNextVideo);

    socket.on('changedList', onChangedList);

    socket.on('hello', onHello);

    socket.on('hello world', function(data) {
        clientId = this.socket.sessionid;
    });
});
